#                 _,.---._    .-._        .--.-. ,--.--------.         
#   _,..---._   ,-.' , -  `. /==/ \  .-._/==/  //==/,  -   , -\
# /==/,   -  \ /==/_,  ,  - \|==|, \/ /, |==\ -\\==\.-.  - ,-./        
# |==|   _   _\==|   .=.     |==|-  \|  | \==\- \`--`\==\- \
# |==|  .=.   |==|_ : ;=:  - |==| ,  | -|  `--`-'     \==\_ \
# |==|,|   | -|==| , '='     |==| -   _ |             |==|- |          
# |==|  '='   /\==\ -    ,_ /|==|  /\ , |             |==|, |          
# |==|-,   _`/  '.='. -   .' /==/, | |- |             /==/ -/          
# `-.`.____.'     `--`--''   `--`./  `--`             `--`--`          
#                    _ __    ,---.      .-._         .=-.-.  _,.----.   
#                 .-`.' ,`..--.'  \    /==/ \  .-._ /==/_ /.' .' -   \
#                /==/, -   \==\-/\ \   |==|, \/ /, /==|, |/==/  ,  ,-'  
#               |==| _ .=. /==/-|_\ |  |==|-  \|  ||==|  ||==|-   |  .  
#               |==| , '=',\==\,   - \ |==| ,  | -||==|- ||==|_   `-' \
#               |==|-  '..'/==/ -   ,| |==| -   _ ||==| ,||==|   _  , | 
#               |==|,  |  /==/-  /\ - \|==|  /\ , ||==|- |\==\.       / 
#               /==/ - |  \==\ _.\=\.-'/==/, | |- |/==/. / `-.`.___.-'  
#               `--`---'   `--`        `--`./  `--``--`-`
#
#	@(#)Copyright (c) 2023, Nathan D. Fisher.
#
#      This is free software.  It comes with NO WARRANTY.
#      Permission to use, modify and distribute this source code
#      is granted subject to the following conditions.
#      1/ that the above copyright notice and this notice
#      are preserved in all copies and that due credit be given
#      to the author.
#      2/ that any changes to this code are clearly commented
#      as such so that the author does not get blamed for bugs
#      other than his own.
#

include config.mk

.SUFFIXES:
.SUFFIXES: .o .c

CFLAGS     += -Wall -Werror
CFLAGS     += -Iinclude
CFLAGS     += -fPIC

hdrs       += include/bytes.h
hdrs       += include/haggis.h
hdrs       += include/jobq.h
hdrs       += include/mq.h

mans       += man/haggis_node.3

srcs       += bytes.c
srcs       += haggis.c
srcs       += jobq.c
srcs       += mq.c
srcs       += linkmap.c

binsrc     += bin.c

objs        = $(srcs:.c=.o)

all: shared static

bin: haggis

shared: libhaggis.so

static: libhaggis.a

$(srcs): $(hdrs)

$(binsrc): $(hdrs) $(srcs)

libhaggis.a: $(objs)
	$(AR) rcs $@ $?

libhaggis.so: $(objs)
	$(CC) -shared -o $@ $? $(LIBS)

haggis: $(binsrc) libhaggis.a
	$(CC) -I./include -o $@ $?

install: install_include install_man install_shared install_static

install_include: include/haggis.h
	@[ -d $(includedir) ] || install -d $(includedir)
	install -m644 include/haggis.h $(includedir)/

install_man: $(mans)
	@[ -d $(mandir)/man3 ] || install -d $(mandir)/man3
	install -m644 $(mans) $(mandir)/man3/

install_static: libhaggis.a
	@[ -d $(libdir) ] || install -d $(libdir)
	install -m644 libhaggis.a $(libdir)/

install_shared: libhaggis.so
	@[ -d $(libdir) ] || install -d $(libdir)
	install -m755 libhaggis.so $(libdir)/

test: libhaggis.a
	$(MAKE) -C test

testclean: clean
	$(MAKE) -C test clean

clean:
	rm -rf *.a *.so *.o haggis

.PHONY: all bin shared static clean install install_include install_man \
    install_static install_shared testclean test
