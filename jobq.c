/*                 _,.---._    .-._        .--.-. ,--.--------.         
 *   _,..---._   ,-.' , -  `. /==/ \  .-._/==/  //==/,  -   , -\
 * /==/,   -  \ /==/_,  ,  - \|==|, \/ /, |==\ -\\==\.-.  - ,-./        
 * |==|   _   _\==|   .=.     |==|-  \|  | \==\- \`--`\==\- \
 * |==|  .=.   |==|_ : ;=:  - |==| ,  | -|  `--`-'     \==\_ \
 * |==|,|   | -|==| , '='     |==| -   _ |             |==|- |          
 * |==|  '='   /\==\ -    ,_ /|==|  /\ , |             |==|, |          
 * |==|-,   _`/  '.='. -   .' /==/, | |- |             /==/ -/          
 * `-.`.____.'     `--`--''   `--`./  `--`             `--`--`          
 *                    _ __    ,---.      .-._         .=-.-.  _,.----.   
 *                 .-`.' ,`..--.'  \    /==/ \  .-._ /==/_ /.' .' -   \
 *                /==/, -   \==\-/\ \   |==|, \/ /, /==|, |/==/  ,  ,-'  
 *               |==| _ .=. /==/-|_\ |  |==|-  \|  ||==|  ||==|-   |  .  
 *               |==| , '=',\==\,   - \ |==| ,  | -||==|- ||==|_   `-' \
 *               |==|-  '..'/==/ -   ,| |==| -   _ ||==| ,||==|   _  , | 
 *               |==|,  |  /==/-  /\ - \|==|  /\ , ||==|- |\==\.       / 
 *               /==/ - |  \==\ _.\=\.-'/==/, | |- |/==/. / `-.`.___.-'  
 *               `--`---'   `--`        `--`./  `--``--`-`
 *
 *	@(#)Copyright (c) 2023, Nathan D. Fisher.
 *
 *      This is free software.  It comes with NO WARRANTY.
 *      Permission to use, modify and distribute this source code
 *      is granted subject to the following conditions.
 *      1/ that the above copyright notice and this notice
 *      are preserved in all copies and that due credit be given
 *      to the author.
 *      2/ that any changes to this code are clearly commented
 *      as such so that the author does not get blamed for bugs
 *      other than his own.
*/
#include <stdlib.h>    // free, malloc

#include "jobq.h"

int haggis_jobq_push(haggis_jobq *self, haggis_job *job) {
    haggis_jobq_node *new = malloc(sizeof(haggis_jobq_node));
    if (new == NULL)
        return 1;
    new->job = job;
    new->next = new->prev = NULL;

    pthread_mutex_lock(self->mutex);
    if (self->tail == NULL) {
        self->tail = self->head = new;
    } else {
        new->next = self->tail;
        self->tail->prev = new;
        self->tail = new;
    }
    self->count++;
    pthread_mutex_unlock(self->mutex);
    return 0;
}

haggis_job* haggis_jobq_pop(haggis_jobq *self) {
    haggis_jobq_node *j;
    haggis_job *job;

    while (self->count == 0)
        pthread_cond_wait(self->cond, self->mutex);
    pthread_mutex_lock(self->mutex);
    self->count--;
    j = self->head;
    job = j->job;
    if (self->tail == self->head) {
        self->tail = self->head = NULL;
    } else {
        self->head = self->head->prev;
    }
    free(j);
    pthread_mutex_unlock(self->mutex);
    return job;
}
