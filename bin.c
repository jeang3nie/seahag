#include "haggis.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

typedef enum {
    action_error,
    command_help,
    create_archive,
    extract_archive,
    list_files,
} haggis_action;

struct option main_opts[] = {
    {"help", no_argument, &help_flag, 'h'},
    {"version", no_argument, &version_flag, 'V'},
    {0, 0, 0, 0},
};
struct option create_opts[] = {
    {"algorithm", required_argument, NULL, 'a'},
    {"quiet", no_argument, &quiet_flag, 'q'},
    {"zstd", no_argument, &zstd_flag, 'z'},
    {"level", required_argument, NULL, 'l'},
    {"help", no_argument, &help_flag, 'h'},
    {0, 0, 0, 0},
};
struct option extract_opts[] = {
    {"quiet", no_argument, &quiet_flag, 'q'},
    {"zstd", no_argument, &zstd_flag, 'z'},
    {"change", required_argument, NULL, 'c'},
    {"help", no_argument, &help_flag, 'h'},
    {0, 0, 0, 0},
};
struct option list_opts[] = {
    {"color", no_argument, &color_flag, 'c'},
    {"long", no_argument, &long_flag, 'l'},
    {"files", no_argument, &files_flag, 'f'},
    {"pager", optional_argument, NULL, 'p'},
    {"zstd", no_argument, &zstd_flag, 'z'},
    {"help", no_argument, &help_flag, 'h'},
    {0, 0, 0, 0},
};

haggis_action parse_action(char *spec) {
    haggis_action action;
    if (memcmp(spec, "help", 4) == 0) {
        action = command_help;
    } else if (memcmp(spec, "create", 6) == 0 || memcmp(spec, "cr", 2) == 0) {
        action = create_archive;
    } else if (memcmp(spec, "extract", 7) == 0 || memcmp(spec, "ex", 2) == 0) {
        action = extract_archive;
    } else if (memcmp(spec, "list", 4) == 0 || memcmp(spec, "ls", 2) == 0) {
        action = list_files;
    } else {
        action = action_error;
    }
    return action;
}

haggis_algorithm parse_algorithm(char *spec) {
    haggis_algorithm alg = skip;
    return alg;
}

void print_help() {
    fprintf(stderr, "todo\n");
}

void print_version() {
    fprintf(stderr, "todo\n");
}

void print_usage() {
    fprintf(stderr, "todo\n");
}

int main(int argc, char **argv) {
    int c, idx = 0, help_flag = 0, version_flag = 0, quiet_flag = 0,
        zstd_flag = 0, long_flag = 0, files_flag = 0, color_flag = 0;

    haggis_action action = no_action;
    char *archive = NULL;
    char *subcommand = NULL;
    if (argc < 2) {
        print_usage();
        exit(1);
    }
    subcommand = *argv;
    if (memcmp(subcommand, "create", 6) == 0) {
        action = create_archive;
        argv++;
    } else if (memcmp(subcommand, "extract", 7) == 0) {
        action = extract_archive;
        argv++;
    } else if (memcmp(subcommand, "list", 4) == 0) {
        action == list_files;
        argv++;
    } else if (memcmp(subcommand, "help", 4) == 0) {
        // todo
        print_help();
        exit(0);
    } else {
        print_usage();
        exit(1);
    }
    archive = *argv;
    
    /*while (1) {
        c = getopt_long(argc, argv, "hvqz", opts, &idx);
        if (c == -1)
            break;
        switch (c) {
            case 'h':
                print_help();
                break;
            case 'v':
                print_version();
                break;
            case '?':
                break;
            default:
                break;
        }
    }*/
    return 0;
}
