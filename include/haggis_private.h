/*                 _,.---._    .-._        .--.-. ,--.--------.         
 *   _,..---._   ,-.' , -  `. /==/ \  .-._/==/  //==/,  -   , -\
 * /==/,   -  \ /==/_,  ,  - \|==|, \/ /, |==\ -\\==\.-.  - ,-./        
 * |==|   _   _\==|   .=.     |==|-  \|  | \==\- \`--`\==\- \
 * |==|  .=.   |==|_ : ;=:  - |==| ,  | -|  `--`-'     \==\_ \
 * |==|,|   | -|==| , '='     |==| -   _ |             |==|- |          
 * |==|  '='   /\==\ -    ,_ /|==|  /\ , |             |==|, |          
 * |==|-,   _`/  '.='. -   .' /==/, | |- |             /==/ -/          
 * `-.`.____.'     `--`--''   `--`./  `--`             `--`--`          
 *                    _ __    ,---.      .-._         .=-.-.  _,.----.   
 *                 .-`.' ,`..--.'  \    /==/ \  .-._ /==/_ /.' .' -   \
 *                /==/, -   \==\-/\ \   |==|, \/ /, /==|, |/==/  ,  ,-'  
 *               |==| _ .=. /==/-|_\ |  |==|-  \|  ||==|  ||==|-   |  .  
 *               |==| , '=',\==\,   - \ |==| ,  | -||==|- ||==|_   `-' \
 *               |==|-  '..'/==/ -   ,| |==| -   _ ||==| ,||==|   _  , | 
 *               |==|,  |  /==/-  /\ - \|==|  /\ , ||==|- |\==\.       / 
 *               /==/ - |  \==\ _.\=\.-'/==/, | |- |/==/. / `-.`.___.-'  
 *               `--`---'   `--`        `--`./  `--``--`-`
 *
 *	@(#)Copyright (c) 2023, Nathan D. Fisher.
 *
 *      This is free software.  It comes with NO WARRANTY.
 *      Permission to use, modify and distribute this source code
 *      is granted subject to the following conditions.
 *      1/ that the above copyright notice and this notice
 *      are preserved in all copies and that due credit be given
 *      to the author.
 *      2/ that any changes to this code are clearly commented
 *      as such so that the author does not get blamed for bugs
 *      other than his own.
*/

#ifndef HAGGIS_PRIVATE_H
#define HAGGIS_PRIVATE_H 1

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include <sys/types.h>

#include "bytes.h"
#include "haggis.h"
#include "mq.h"

int      haggis_store_header   (FILE *stream);
int      haggis_check_header   (FILE *stream);
void     haggis_device_init    (haggis_device *self, dev_t rdev);
int      haggis_store_device   (haggis_device *self, FILE *stream);
int      haggis_load_device    (haggis_device *self, FILE *stream);
int      haggis_store_cksum    (haggis_checksum *self, FILE *stream);
int      haggis_load_cksum     (haggis_checksum *self, FILE *stream);
int      validate_md5          (haggis_file *file);
int      validate_sha1         (haggis_file *file);
int      validate_sha256       (haggis_file *file);
int      haggis_validate_cksum (haggis_file *file);
int      haggis_file_init      (haggis_file *self, char *path, haggis_algorithm a);
int      haggis_store_file     (haggis_file *self, FILE *stream);
int      haggis_load_file      (haggis_file *self, FILE *stream);
void     haggis_filename_init  (char *target, haggis_filename *fname);
void     haggis_filename_deinit(haggis_filename *fname);
int      haggis_load_filename  (FILE *stream, haggis_filename *n);
int      haggis_store_filename (FILE *stream, haggis_filename *n);
int      haggis_load_filetype  (FILE *stream, haggis_typeflag tag, haggis_filetype *file);
int      haggis_store_filetype (FILE *stream, haggis_filetype *filetype);
uint64_t hash_fnv1a_64         (uint8_t *key, size_t len);
uint64_t hash_str_fnv1a_64     (char *s);

struct _map_node {
    struct _map_node *next;
    void             *key;
    uint64_t          hash;
    void             *data;
};

typedef struct _map_node hmap_node;

typedef struct {
    size_t     capacity;
    size_t     len;
    size_t     keysize;
    hmap_node *buckets;
} hmap;

hmap_node* hmap_node_init  (void *key, size_t keysize, void *data);
void       hmap_node_deinit(hmap_node *self);
void*      hmap_node_attach(hmap_node *root, hmap_node *leaf, size_t keysize);
void*      hmap_node_search(hmap_node *root, void *key, size_t keysize);
hmap*      hmap_init       (size_t keysize);
void       hmap_deinit     (hmap *self);
int        hmap_expand     (hmap *self);
void*      hmap_insert     (hmap *self, void *key, void *data);
void*      hmap_get        (hmap *self, void *key);
void*      hmap_remove     (hmap *self, void *key);

#endif // !HAGGIS_PRIVATE_H
