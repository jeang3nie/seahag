/*                 _,.---._    .-._        .--.-. ,--.--------.         
 *   _,..---._   ,-.' , -  `. /==/ \  .-._/==/  //==/,  -   , -\
 * /==/,   -  \ /==/_,  ,  - \|==|, \/ /, |==\ -\\==\.-.  - ,-./        
 * |==|   _   _\==|   .=.     |==|-  \|  | \==\- \`--`\==\- \
 * |==|  .=.   |==|_ : ;=:  - |==| ,  | -|  `--`-'     \==\_ \
 * |==|,|   | -|==| , '='     |==| -   _ |             |==|- |          
 * |==|  '='   /\==\ -    ,_ /|==|  /\ , |             |==|, |          
 * |==|-,   _`/  '.='. -   .' /==/, | |- |             /==/ -/          
 * `-.`.____.'     `--`--''   `--`./  `--`             `--`--`          
 *                    _ __    ,---.      .-._         .=-.-.  _,.----.   
 *                 .-`.' ,`..--.'  \    /==/ \  .-._ /==/_ /.' .' -   \
 *                /==/, -   \==\-/\ \   |==|, \/ /, /==|, |/==/  ,  ,-'  
 *               |==| _ .=. /==/-|_\ |  |==|-  \|  ||==|  ||==|-   |  .  
 *               |==| , '=',\==\,   - \ |==| ,  | -||==|- ||==|_   `-' \
 *               |==|-  '..'/==/ -   ,| |==| -   _ ||==| ,||==|   _  , | 
 *               |==|,  |  /==/-  /\ - \|==|  /\ , ||==|- |\==\.       / 
 *               /==/ - |  \==\ _.\=\.-'/==/, | |- |/==/. / `-.`.___.-'  
 *               `--`---'   `--`        `--`./  `--``--`-`
 *
 *	@(#)Copyright (c) 2023, Nathan D. Fisher.
 *
 *      This is free software.  It comes with NO WARRANTY.
 *      Permission to use, modify and distribute this source code
 *      is granted subject to the following conditions.
 *      1/ that the above copyright notice and this notice
 *      are preserved in all copies and that due credit be given
 *      to the author.
 *      2/ that any changes to this code are clearly commented
 *      as such so that the author does not get blamed for bugs
 *      other than his own.
*/

#ifndef BYTES_H
#define BYTES_H 1

#include <stdio.h>        // FILE

#include "haggis.h"

int load_u16 (FILE *stream, u16 *num);
int store_u16(FILE *stream, u16 *num);
int load_u32 (FILE *stream, u32 *num);
int store_u32(FILE *stream, u32 *num);
int load_u64 (FILE *stream, u64 *num);
int store_u64(FILE *stream, u64 *num);

#endif
