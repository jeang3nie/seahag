/*                 _,.---._    .-._        .--.-. ,--.--------.         
 *   _,..---._   ,-.' , -  `. /==/ \  .-._/==/  //==/,  -   , -\
 * /==/,   -  \ /==/_,  ,  - \|==|, \/ /, |==\ -\\==\.-.  - ,-./        
 * |==|   _   _\==|   .=.     |==|-  \|  | \==\- \`--`\==\- \
 * |==|  .=.   |==|_ : ;=:  - |==| ,  | -|  `--`-'     \==\_ \
 * |==|,|   | -|==| , '='     |==| -   _ |             |==|- |          
 * |==|  '='   /\==\ -    ,_ /|==|  /\ , |             |==|, |          
 * |==|-,   _`/  '.='. -   .' /==/, | |- |             /==/ -/          
 * `-.`.____.'     `--`--''   `--`./  `--`             `--`--`          
 *                    _ __    ,---.      .-._         .=-.-.  _,.----.   
 *                 .-`.' ,`..--.'  \    /==/ \  .-._ /==/_ /.' .' -   \
 *                /==/, -   \==\-/\ \   |==|, \/ /, /==|, |/==/  ,  ,-'  
 *               |==| _ .=. /==/-|_\ |  |==|-  \|  ||==|  ||==|-   |  .  
 *               |==| , '=',\==\,   - \ |==| ,  | -||==|- ||==|_   `-' \
 *               |==|-  '..'/==/ -   ,| |==| -   _ ||==| ,||==|   _  , | 
 *               |==|,  |  /==/-  /\ - \|==|  /\ , ||==|- |\==\.       / 
 *               /==/ - |  \==\ _.\=\.-'/==/, | |- |/==/. / `-.`.___.-'  
 *               `--`---'   `--`        `--`./  `--``--`-`
 *
 *	@(#)Copyright (c) 2023, Nathan D. Fisher.
 *
 *      This is free software.  It comes with NO WARRANTY.
 *      Permission to use, modify and distribute this source code
 *      is granted subject to the following conditions.
 *      1/ that the above copyright notice and this notice
 *      are preserved in all copies and that due credit be given
 *      to the author.
 *      2/ that any changes to this code are clearly commented
 *      as such so that the author does not get blamed for bugs
 *      other than his own.
*/

#ifndef HAGGIS_H
#define HAGGIS_H 1

#include <pthread.h>       // pthread_mutex_t
#include <stdint.h>        // uint<x>_t
#include <stdio.h>         // FILE
#include <sys/types.h>     // ino_t

typedef uint8_t u8;

typedef union {
    uint16_t val;
    u8       bytes[2];
} u16;

typedef union {
    uint32_t val;
    u8       bytes[4];
} u32;

typedef union {
    uint64_t val;
    u8       bytes[8];
} u64;

typedef struct {
    u32 major;
    u32 minor;
} haggis_device;

typedef enum {
    md5,
    sha1,
    sha256,
    skip,
} haggis_algorithm;

typedef struct {
    haggis_algorithm tag;
    union {
        u8           md5[16];
        u8           sha1[20];
        u8           sha256[32];
    };
} haggis_checksum;

typedef struct {
    u64              len;
    haggis_checksum  cksum;
    u8              *data;
} haggis_file;

typedef struct {
    u16   len;
    char *name;
} haggis_filename;

typedef enum {
    normal,
    hardlink,
    softlink,
    directory,
    character,
    block,
    fifo,
    eof,
} haggis_typeflag;

typedef struct {
    haggis_typeflag     tag;
    union {
        haggis_file     file;
        haggis_filename target;
        haggis_device   dev;
    };
} haggis_filetype;

typedef struct {
    haggis_filename name;
    u32             uid;
    u32             gid;
    u64             mtime;
    u16             mode;
    haggis_filetype filetype;
} haggis_node;

#define HAGGIS_BUCKETS_BASE 64

typedef struct {
    ino_t       key;
    uint64_t    hash;
    char       *path;
} haggis_bucket;

typedef struct {
    pthread_mutex_t mutex;
    size_t          len;
    size_t          capacity;
    haggis_bucket  *buckets;
} haggis_linkmap;

typedef enum {
    DevNodeSkipped,
    NodeCreated,
    NodeExtracted,
    EndOfArchive,
    ArchiveError,
} haggis_message_type;

typedef union {
    char *f_name;
    int   err;
} haggis_message_body;

struct _mq_node {
    struct _mq_node    *prev;
    struct _mq_node    *next;
    haggis_message_type tag;
    haggis_message_body body;
};

typedef struct _mq_node haggis_msg;

typedef struct {
    pthread_cond_t   cond;
    size_t           count;
    pthread_mutex_t  mutex;
    haggis_msg      *head;
    haggis_msg      *tail;
} haggis_mq;

haggis_linkmap* haggis_linkmap_init      ();
void            haggis_linkmap_deinit    (haggis_linkmap *self);
char*           haggis_linkmap_get_or_add(haggis_linkmap *self, ino_t inode, char *path);
void            haggis_node_deinit       (haggis_node *self);
haggis_node*    haggis_create_node       (char *file, haggis_algorithm a, haggis_linkmap *map,
                                          haggis_mq *mq);
int             haggis_extract_node      (haggis_node *self, char *basedir, haggis_mq *mq);
int             haggis_load_node         (haggis_node *self, FILE *stream);
int             haggis_store_node        (haggis_node *self, FILE *stream);

#endif // !HAGGIS_H
