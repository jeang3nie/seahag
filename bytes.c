/*                 _,.---._    .-._        .--.-. ,--.--------.
 *   _,..---._   ,-.' , -  `. /==/ \  .-._/==/  //==/,  -   , -\
 * /==/,   -  \ /==/_,  ,  - \|==|, \/ /, |==\ -\\==\.-.  - ,-./
 * |==|   _   _\==|   .=.     |==|-  \|  | \==\- \`--`\==\- \
 * |==|  .=.   |==|_ : ;=:  - |==| ,  | -|  `--`-'     \==\_ \
 * |==|,|   | -|==| , '='     |==| -   _ |             |==|- |
 * |==|  '='   /\==\ -    ,_ /|==|  /\ , |             |==|, |
 * |==|-,   _`/  '.='. -   .' /==/, | |- |             /==/ -/
 * `-.`.____.'     `--`--''   `--`./  `--`             `--`--`
 *                    _ __    ,---.      .-._         .=-.-.  _,.----.
 *                 .-`.' ,`..--.'  \    /==/ \  .-._ /==/_ /.' .' -   \
 *                /==/, -   \==\-/\ \   |==|, \/ /, /==|, |/==/  ,  ,-'
 *               |==| _ .=. /==/-|_\ |  |==|-  \|  ||==|  ||==|-   |  .
 *               |==| , '=',\==\,   - \ |==| ,  | -||==|- ||==|_   `-' \
 *               |==|-  '..'/==/ -   ,| |==| -   _ ||==| ,||==|   _  , |
 *               |==|,  |  /==/-  /\ - \|==|  /\ , ||==|- |\==\.       /
 *               /==/ - |  \==\ _.\=\.-'/==/, | |- |/==/. / `-.`.___.-'
 *               `--`---'   `--`        `--`./  `--``--`-`
 *
 *	@(#)Copyright (c) 2023, Nathan D. Fisher.
 *
 *      This is free software.  It comes with NO WARRANTY.
 *      Permission to use, modify and distribute this source code
 *      is granted subject to the following conditions.
 *      1/ that the above copyright notice and this notice
 *      are preserved in all copies and that due credit be given
 *      to the author.
 *      2/ that any changes to this code are clearly commented
 *      as such so that the author does not get blamed for bugs
 *      other than his own.
 */

#include <endian.h>          // __BYTE_ORDER__ / __LITTLE_ENDIAN macros
#include <stdio.h>           // FILE
#include <stdint.h>          // uint<x>_t

#include "bytes.h"
#include "haggis.h"

#if __BYTE_ORDER__ == __LITTLE_ENDIAN || _BYTE_ORDER == _LITTLE_ENDIAN
int load_u16(FILE *stream, u16 *num) {
    return fread(&num->bytes, 1, 2, stream);
}

int store_u16(FILE *stream, u16 *num) {
    return fwrite(&num->bytes, 1, 2, stream);
}

int load_u32(FILE *stream, u32 *num) {
    return fread(&num->bytes, 1, 4, stream);
}

int store_u32(FILE *stream, u32 *num) {
    return fwrite(&num->bytes, 1, 4, stream);
}

int load_u64(FILE *stream, u64 *num) {
    return fread(&num->bytes, 1, 8, stream);
}

int store_u64(FILE *stream, u64 *num) {
    return fwrite(&num->bytes, 1, 8, stream);
}
#else
int load_u16(FILE *stream, u16 *num) {
    u8 buf[2];
    int res = fread(buf, 1, 2, stream);
    if (res != 2)
	return res;
    num->bytes[0] = buf[1];
    num->bytes[1] = buf[0];
    return res;
}

int store_u16(FILE * stream, u16 *num) {
    u8 buf[2];
    buf[0] = num->bytes[1];
    buf[1] = num->bytes[0];
    return fwrite(buf, 1, 2, stream);
}

int load_u32(FILE *stream, u32 *num) {
    u8 buf[4];
    int res = fread(buf, 1, 4, stream);
    if (res != 4)
	return res;
    num->bytes[0] = buf[3];
    num->bytes[1] = buf[2];
    num->bytes[2] = buf[1];
    num->bytes[3] = buf[0];
}

int store_u32(FILE *stream, u32 *num) {
    u8 buf[4];
    buf[0] = num->bytes[3];
    buf[1] = num->bytes[2];
    buf[2] = num->bytes[1];
    buf[3] = num->bytes[0];
    return fwrite(buf, 1, 2, stream);
}

int load_u64(FILE *stream, u64 *num) {
    u8 buf[8];
    int res = fread(buf, 1, 8, stream);
    if (res != 8)
	return res;
    num->bytes[0] = buf[7];
    num->bytes[1] = buf[6];
    num->bytes[2] = buf[5];
    num->bytes[3] = buf[4];
    num->bytes[4] = buf[3];
    num->bytes[5] = buf[2];
    num->bytes[6] = buf[1];
    num->bytes[7] = buf[0];
}

int store_u64(FILE *stream, u64 *num) {
    u8 buf[8];
    buf[0] = num->bytes[7];
    buf[1] = num->bytes[6];
    buf[2] = num->bytes[5];
    buf[3] = num->bytes[4];
    buf[4] = num->bytes[3];
    buf[5] = num->butes[2];
    buf[6] = num->bytes[1];
    buf[7] = num->bytes[0];
    return fwrite(buf, 1, 2, stream);
}
#endif
