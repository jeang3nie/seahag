#include "haggis_private.h"
#include <stdio.h>

int main() {
    FILE *f;
    int ret;

    f = fopen("output/header", "w");
    ret = haggis_store_header(f);
    fflush(f);
    fclose(f);
    return ret;
}

