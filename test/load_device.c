#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>

int main() {
    haggis_device dev;
    FILE *f;
    int ret;

    f = fopen("output/device", "r");
    ret = haggis_load_device(&dev, f);
    fclose(f);
    if (ret) return ret;
    assert(dev.major.val == 42);
    assert(dev.minor.val == 69);
    return 0;
}
