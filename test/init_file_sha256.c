#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    haggis_file hf;
    char *f = "check_header.c";
    const u8 buf[32] = { 254, 29, 31, 135, 226, 153, 9, 124, 189, 232, 10, 158,
        250, 203, 77, 108, 230, 7, 159, 124, 6, 237, 169, 239, 70, 218, 173, 188,
        153, 219, 121, 82 };

    assert(haggis_file_init(&hf, f, sha256) == 0);
    assert(hf.cksum.tag == sha256);
    assert(memcmp(buf, hf.cksum.sha256, 32) == 0);
}

