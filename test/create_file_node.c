#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "haggis.h"
#include "mq.h"

int main() {
   haggis_linkmap *map;
   haggis_node    *node;
   haggis_mq       mq;
   char           *path = "create_dev_node.c";
   uint8_t         sum[] = { 0x69, 0x05, 0x0c, 0x52, 0x1d, 0xc9, 0x5e, 0x8d, 0x29,
                             0x42, 0xb3, 0x77, 0x81, 0x6f, 0x00, 0x47, 0x19, 0xd3,
                             0x67, 0x03, 0xd5, 0xaa, 0xce, 0x4f, 0x3f, 0x40, 0xc0,
                             0x52, 0x54, 0xc1, 0x5b, 0x13 };
   int i;

   map = haggis_linkmap_init();
   assert(map != NULL);
   assert(haggis_mq_init(&mq) == 0);
   node = haggis_create_node(path, sha256, map, &mq);
   assert(node->filetype.tag == normal);
   for (i = 0; i < 32; i++) {
      assert(sum[i] == node->filetype.file.cksum.sha256[i]);
   }
   assert(node->filetype.file.len.val == 726);
   assert(memcmp(path, node->name.name, 17) == 0);
   haggis_node_deinit(node);
   haggis_linkmap_deinit(map);
   return 0;
}
