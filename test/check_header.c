#include "haggis_private.h"
#include <stdio.h>

int main() {
    FILE *f;
    int ret;

    f = fopen("output/header", "r");
    ret = haggis_check_header(f);
    fclose(f);
    return ret;
}

