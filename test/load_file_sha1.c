#include "haggis_private.h"
#include <assert.h>
#include <md5.h>
#include <stdio.h>

int main() {
    haggis_file hf;
    char *f = "output/store_file_sha1";
    FILE *fd;
    int res;

    fd = fopen(f, "r");
    res = haggis_load_file(&hf, fd);
    fclose(fd);
    return res;
}
