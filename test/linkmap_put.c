#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include "haggis.h"
#include "haggis_private.h"

int main() {
    haggis_linkmap *map;
    struct stat s0, s1;
    char *path;
    char *tgt = "Makefile";
    char *lnk = "output/Makefile";

    map = haggis_linkmap_init();
    assert(map != NULL);
    assert(stat(tgt, &s0) == 0);

    if (access(lnk, AT_EACCESS) != 0)
        link(tgt, lnk);

    assert(stat(lnk, &s1) == 0);
    path = haggis_linkmap_get_or_add(map, s0.st_ino, tgt);
    assert(path == NULL);
    path = haggis_linkmap_get_or_add(map, s1.st_ino, lnk);
    assert(memcmp(path, tgt, 9) == 0);
    haggis_linkmap_deinit(map);
    unlink(lnk);
}
