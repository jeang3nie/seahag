#include "bytes.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>

int main() {
    u64 n;
    FILE *fd;

    n.val = 0;
    fd = fopen("output/u64", "r");
    assert(load_u64(fd, &n) == 8);
    assert(n.val == 1234567890);
    fclose(fd);
}

