#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

#include "haggis.h"
#include "mq.h"

int main() {
   haggis_linkmap *map;
   haggis_node    *node;
   haggis_mq       mq;
   char           *path = "output/fifo";

   map = haggis_linkmap_init();
   assert(map != NULL);
   assert(haggis_mq_init(&mq) == 0);
   unlink(path);
   assert(mkfifo(path, 0644) == 0);
   node = haggis_create_node(path, sha256, map, &mq);
   assert(node->filetype.tag == fifo);
   assert(memcmp(path, node->name.name, 11) == 0);
   haggis_node_deinit(node);
   haggis_linkmap_deinit(map);
   return 0;
}