/*                 _,.---._    .-._        .--.-. ,--.--------.
 *   _,..---._   ,-.' , -  `. /==/ \  .-._/==/  //==/,  -   , -\
 * /==/,   -  \ /==/_,  ,  - \|==|, \/ /, |==\ -\\==\.-.  - ,-./
 * |==|   _   _\==|   .=.     |==|-  \|  | \==\- \`--`\==\- \
 * |==|  .=.   |==|_ : ;=:  - |==| ,  | -|  `--`-'     \==\_ \
 * |==|,|   | -|==| , '='     |==| -   _ |             |==|- |
 * |==|  '='   /\==\ -    ,_ /|==|  /\ , |             |==|, |
 * |==|-,   _`/  '.='. -   .' /==/, | |- |             /==/ -/
 * `-.`.____.'     `--`--''   `--`./  `--`             `--`--`
 *                    _ __    ,---.      .-._         .=-.-.  _,.----.
 *                 .-`.' ,`..--.'  \    /==/ \  .-._ /==/_ /.' .' -   \
 *                /==/, -   \==\-/\ \   |==|, \/ /, /==|, |/==/  ,  ,-'
 *               |==| _ .=. /==/-|_\ |  |==|-  \|  ||==|  ||==|-   |  .
 *               |==| , '=',\==\,   - \ |==| ,  | -||==|- ||==|_   `-' \
 *               |==|-  '..'/==/ -   ,| |==| -   _ ||==| ,||==|   _  , |
 *               |==|,  |  /==/-  /\ - \|==|  /\ , ||==|- |\==\.       /
 *               /==/ - |  \==\ _.\=\.-'/==/, | |- |/==/. / `-.`.___.-'
 *               `--`---'   `--`        `--`./  `--``--`-`
 *
 *	@(#)Copyright (c) 2024, Nathan D. Fisher.
 *
 *      This is free software.  It comes with NO WARRANTY.
 *      Permission to use, modify and distribute this source code
 *      is granted subject to the following conditions.
 *      1/ that the above copyright notice and this notice
 *      are preserved in all copies and that due credit be given
 *      to the author.
 *      2/ that any changes to this code are clearly commented
 *      as such so that the author does not get blamed for bugs
 *      other than his own.
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int i, pass = 0, fail = 0, skip = 0, total, ret, read = 0;
    FILE *pipe;
    char cmd[100], output[500];

    total = argc-1;
    printf("\n\t=== \033[0;33mRunning %i tests\033[0m ===\n\n", total);
    for (i = 1; i < argc; i++) {
        snprintf(cmd, 100, "./%s", argv[i]);
        printf("\033[0;2m[\033[0m%03i\033[0;2m/\033[0m%03i\033[0;2m]\033[0m %-25s", i, total, argv[i]);
        pipe = popen(cmd, "w");
        read = fread(&output, 1, 500, pipe);
        ret = pclose(pipe);
        if (read) {
            fail++;
            printf("\033[0;31mFailed\033[0m\n");
            fprintf(stderr, "%s\n", &output);
            continue;
        }
        switch (WEXITSTATUS(ret)) {
            case 0:
                pass++;
                printf("\033[0;32mSuccess\033[0m\n");
                break;
            case 255:
                skip++;
                printf("Skipped\n");
                break;
            default:
                fail++;
                printf("\033[0;31mFailed\033[0m\n");
        }
    }
    if (fail) {
        printf("\nResults: \033[0;31mFAILED\033[0m %i succeeded; %i failed; %i skipped\n\n",
               pass, fail, skip);
    } else {
        printf("\nResults: \033[0;32mOk\033[0m %i succeeded; %i failed; %i skipped\n\n",
               pass, fail, skip);
    }
    return fail;
}
