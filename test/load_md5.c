#include "haggis.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

int main() {
    haggis_checksum cksum;
    FILE *f;
    int i, ret;

    f = fopen("output/md5", "r");
    if (haggis_load_cksum(&cksum, f)) return 1;
    assert(cksum.tag == md5);
    for (i = 0; i < 16; i++) {
        assert(cksum.md5[i] == (uint8_t)i);
    }
    fclose(f);
}
