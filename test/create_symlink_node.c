#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "haggis.h"
#include "mq.h"

int main() {
    haggis_linkmap *map;
    haggis_node    *node;
    haggis_mq       mq;
    char           *path = "output/dev";

    map = haggis_linkmap_init();
    assert(map != NULL);
    assert(haggis_mq_init(&mq) == 0);
    symlink("device", "output/dev");
    node = haggis_create_node(path, sha256, map, &mq);
    assert(node->filetype.tag == softlink);
    assert(memcmp(path, node->name.name, 10) == 0);
    assert(memcmp(node->filetype.target.name, "device", 6) == 0);
    haggis_node_deinit(node);
    haggis_linkmap_deinit(map);
    return 0;
}
