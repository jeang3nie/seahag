#include "bytes.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>

int main() {
    u32 n;
    FILE *fd;

    n.val = 0;
    fd = fopen("output/u32", "r");
    assert(load_u32(fd, &n) == 4);
    assert(n.val == 123456);
    fclose(fd);
}

