#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    haggis_file hf;
    char *f = "check_header.c";
    const u8 buf[20] = { 175, 45, 7, 226, 140, 184, 187, 69, 59, 208, 246, 108,
        187, 249, 63, 111, 56, 65, 1, 58 };

    assert(haggis_file_init(&hf, f, sha1) == 0);
    assert(hf.cksum.tag == sha1);
    assert(memcmp(buf, hf.cksum.sha1, 20) == 0);
}
