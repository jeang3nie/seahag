#include "haggis_private.h"
#include <assert.h>
#include <stdint.h>

int main() {
    uint64_t ret;
    union {
        ino_t val;
        u8 bytes[sizeof(ino_t)];
    } nod;
    nod.val = 4269;

    ret = hash_fnv1a_64(&nod.bytes[0], sizeof(ino_t));
    assert(ret == 5664182581703653080);
}
