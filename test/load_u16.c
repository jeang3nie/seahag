#include "bytes.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>

int main() {
    u16 n;
    FILE *fd;

    n.val = 0;
    fd = fopen("output/u16", "r");
    assert(load_u16(fd, &n) == 2);
    assert(n.val == 300);
    fclose(fd);
}

