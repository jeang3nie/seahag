#include "haggis.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

int main() {
    haggis_checksum cksum;
    FILE *f;
    int i, ret;

    f = fopen("output/sha256", "r");
    if (haggis_load_cksum(&cksum, f)) return 1;
    assert(cksum.tag == sha256);
    for (i = 0; i < 32; i++) {
        assert(cksum.sha256[i] == (uint8_t)i);
    }
    fclose(f);
}

