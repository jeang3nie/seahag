#include "haggis.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

int main() {
    haggis_checksum cksum;
    FILE *f;
    int i, ret;

    f = fopen("output/sha1", "r");
    if (haggis_load_cksum(&cksum, f)) return 1;
    assert(cksum.tag == sha1);
    for (i = 0; i < 20; i++) {
        assert(cksum.sha1[i] == (uint8_t)i);
    }
    fclose(f);
}

