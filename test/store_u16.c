#include "bytes.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>

int main() {
    u16 n;
    FILE *fd;

    n.val = 300;
    fd = fopen("output/u16", "w");
    assert(store_u16(fd, &n) == 2);
    fflush(fd);
    fclose(fd);
}
