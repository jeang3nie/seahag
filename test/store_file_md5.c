#include "haggis_private.h"
#include <assert.h>
#include <md5.h>
#include <stdio.h>

int main() {
    haggis_file hf;
    char *f = "load_device.c";
    FILE *fd;

    assert(haggis_file_init(&hf, f, md5) == 0);
    fd = fopen("output/store_file_md5", "w");
    assert(haggis_store_file(&hf, fd) == 0);
    fflush(fd);
    fclose(fd);
    return 0;
}

