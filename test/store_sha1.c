#include "haggis.h"
#include "haggis_private.h"
#include <stdint.h>
#include <stdio.h>

int main() {
    haggis_checksum cksum;
    FILE *f;
    int i, ret;

    cksum.tag = sha1;
    for (i = 0; i < 20; i++) {
        cksum.md5[i] = (uint8_t)i;
    }
    f = fopen("output/sha1", "w");
    ret = haggis_store_cksum(&cksum, f);
    fclose(f);
    return ret;
}

