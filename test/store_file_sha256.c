#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>

#if defined(__FreeBSD__) || defined(__DragonFly__)
#include <sha256.h>
#elif defined(__NetBSD__) || defined(__OpenBSD__) || defined(__LINUX__)
#include <sha2.h>
#endif

int main() {
    haggis_file hf;
    char *f = "load_device.c";
    FILE *fd;

    assert(haggis_file_init(&hf, f, sha1) == 0);
    fd = fopen("output/store_file_sha256", "w");
    assert(haggis_store_file(&hf, fd) == 0);
    fflush(fd);
    fclose(fd);
    return 0;
}

