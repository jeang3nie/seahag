#include "bytes.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

int main() {
    u32 n;
    FILE *fd;

    n.val = 123456;
    fd = fopen("output/u32", "w");
    assert(store_u32(fd, &n) == 4);
    fflush(fd);
    fclose(fd);
}

