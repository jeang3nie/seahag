#include <errno.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "haggis.h"
#include "mq.h"

int main() {
    haggis_node    *node = NULL;
    haggis_linkmap *map  = NULL;
    haggis_mq       mq;
    haggis_msg     *msg;
    char           *path = "/dev/null";
    int             ret  = 0;

    if (geteuid() != 0)
        return -1;
    map = haggis_linkmap_init();
    assert(map != NULL);
    if (haggis_mq_init(&mq))
        return errno;
    node = haggis_create_node(path, sha256, map, &mq);
    assert(node != NULL);
    assert(node->filetype.tag == block || node->filetype.tag == character);
    assert(memcmp(node->name.name, "dev/null", 8) == 0);
    msg = haggis_mq_pop(&mq);
    assert(msg->tag == NodeCreated);
    assert(memcmp(msg->body.f_name, "dev/null", 8) == 0);
    haggis_msg_deinit(msg);
    ret = haggis_extract_node(node, "output/extracted", &mq);
    msg = haggis_mq_pop(&mq);
    assert(msg->tag == NodeExtracted);
    assert(memcmp(msg->body.f_name, "dev/null", 8) == 0);
    haggis_msg_deinit(msg);
    haggis_node_deinit(node);
    haggis_linkmap_deinit(map);
    return 0;
}
