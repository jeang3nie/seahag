#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "haggis.h"
#include "mq.h"

int main() {
    haggis_node    *node = NULL;
    haggis_linkmap *map  = NULL;
    haggis_mq       mq;
    haggis_msg     *msg  = NULL;
    char           *path = "output";
    int             ret = 0;

    map = haggis_linkmap_init();
    assert(map != NULL);
    assert(haggis_mq_init(&mq) == 0);
    node = haggis_create_node(path, skip, map, &mq);
    assert(node != NULL);
    assert(node->filetype.tag == directory);
    assert(memcmp(node->name.name, path, 6) == 0);
    msg = haggis_mq_pop(&mq);
    assert(msg->tag == NodeCreated);
    assert(memcmp(msg->body.f_name, path, 6) == 0);
    haggis_msg_deinit(msg);
    ret = haggis_extract_node(node, "output/extracted", &mq);
    assert(ret == 0);
    msg = haggis_mq_pop(&mq);
    assert(msg->tag == NodeExtracted);
    assert(memcmp(msg->body.f_name, path, 6) == 0);
    haggis_msg_deinit(msg);
    haggis_node_deinit(node);
    haggis_linkmap_deinit(map);
    return 0;
}
