#include "haggis_private.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    haggis_file hf;
    char *f = "check_header.c";
    const u8 buf[16] = { 127, 161, 170, 134, 33, 218, 100, 29, 250, 255, 15, 125, 109, 5, 216, 9 };

    assert(haggis_file_init(&hf, f, md5) == 0);
    assert(hf.cksum.tag == md5);
    assert(memcmp(buf, hf.cksum.md5, 16) == 0);
}
