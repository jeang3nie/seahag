#include "haggis_private.h"
#include <assert.h>
#include <stdint.h>

int main() {
    uint64_t ret;
    char *key = "haggis";

    ret = hash_str_fnv1a_64(key);
    assert(ret == 4320822873175422592);
}
