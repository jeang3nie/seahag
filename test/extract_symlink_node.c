#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "haggis.h"
#include "mq.h"

int main() {
    haggis_node    *node = NULL;
    haggis_linkmap *map  = NULL;
    haggis_mq       mq;
    haggis_msg     *msg  = NULL;
    char           *tgt  = "../Makefile", *lnk = "output/test.mk";
    int             ret  = 0;

    map = haggis_linkmap_init();
    assert(map != NULL);
    assert(haggis_mq_init(&mq) == 0);
    unlink(lnk);
    assert(symlink(tgt, lnk) == 0);
    node = haggis_create_node(lnk, skip, map, &mq);
    assert(node != NULL);
    assert(node->filetype.tag == softlink);
    assert(memcmp(node->name.name, lnk, 14) == 0);
    assert(memcmp(node->filetype.target.name, tgt, 11) == 0);
    ret = haggis_extract_node(node, "output/extracted", &mq);
    assert(ret == 0);
    msg = haggis_mq_pop(&mq);
    assert(msg->tag == NodeCreated);
    assert(memcmp(msg->body.f_name, lnk, 14) == 0);
    haggis_msg_deinit(msg);
    msg = haggis_mq_pop(&mq);
    assert(msg->tag == NodeExtracted);
    assert(memcmp(msg->body.f_name, lnk, 14) == 0);
    assert(mq.head == NULL);
    assert(mq.tail == NULL);
    haggis_msg_deinit(msg);
    haggis_node_deinit(node);
    haggis_linkmap_deinit(map);
    return 0;
}
