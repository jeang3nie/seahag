#include "haggis_private.h"
#include <stdio.h>

int main() {
    haggis_device  dev;
    FILE          *f;
    int            ret;

    dev.major.val = 42;
    dev.minor.val = 69;
    f = fopen("output/device", "w");
    ret = haggis_store_device(&dev, f);
    fflush(f);
    fclose(f);
    return ret;
}
