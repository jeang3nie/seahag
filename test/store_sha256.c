#include "haggis.h"
#include "haggis_private.h"
#include <stdint.h>
#include <stdio.h>

int main() {
    haggis_checksum cksum;
    FILE *f;
    int i, ret;

    cksum.tag = sha256;
    for (i = 0; i < 32; i++) {
        cksum.sha256[i] = (uint8_t)i;
    }
    f = fopen("output/sha256", "w");
    ret = haggis_store_cksum(&cksum, f);
    fclose(f);
    return ret;
}


