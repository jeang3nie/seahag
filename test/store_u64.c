#include "bytes.h"
#include "haggis_private.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

int main() {
    u64 n;
    FILE *fd;

    n.val = 1234567890;
    fd = fopen("output/u64", "w");
    assert(store_u64(fd, &n) == 8);
    fflush(fd);
    fclose(fd);
}

