#include <assert.h>
#include <sys/stat.h>

#include "haggis.h"
#include "mq.h"

int main() {
   haggis_linkmap *map;
   haggis_node    *node;
   haggis_mq       mq;
   char           *path = "/dev/null";

   map = haggis_linkmap_init();
   assert(map != NULL);
   assert(haggis_mq_init(&mq) == 0);
   node = haggis_create_node(path, sha256, map, &mq);
   assert(node->filetype.tag == character || node->filetype.tag == block);
#if defined(__linux__)
   assert(node->filetype.dev.major.val == 1);
   assert(node->filetype.dev.minor.val == 3);
#elif defined(__FreeBSD__)
   assert(node->filetype.dev.major.val == 0);
#endif /* if defined (__linux__) */
   haggis_node_deinit(node);
   haggis_linkmap_deinit(map);
   return 0;
}
