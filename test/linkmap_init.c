#include <assert.h>
#include <stdio.h>

#include "haggis.h"
#include "haggis_private.h"

int main() {
    haggis_linkmap *map;

    map = haggis_linkmap_init();
    assert(map != NULL);
    haggis_linkmap_deinit(map);
}
