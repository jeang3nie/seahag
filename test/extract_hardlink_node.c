#include <errno.h>
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "haggis.h"
#include "mq.h"

haggis_node    *node0 = NULL, *node1 = NULL;
haggis_linkmap *map   = NULL;

void log_and_die(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    if (map != NULL)
        haggis_linkmap_deinit(map);
    if (node0 != NULL)
        haggis_node_deinit(node0);
    if (node1 != NULL)
        haggis_node_deinit(node1);
    exit(EXIT_FAILURE);
}

int main() {
    const char     *orig  = "Makefile";
    const char     *lnk   = "output/Makefile";
    haggis_mq       mq;
    haggis_msg     *msg;
    int             ret   = 0;

    map = haggis_linkmap_init();
    if (map == NULL)
        log_and_die("OOM while creating linkmap\n");
    if (haggis_mq_init(&mq))
        return errno;
    unlink(lnk);
    ret = link(orig, lnk);
    if (ret) return ret;
    node0 = haggis_create_node("Makefile", sha256, map, &mq);
    if (node0 == NULL)
        log_and_die("OOM while creating Node 0\n");
    if (node0->filetype.tag != normal)
        log_and_die("Node 0 incorrect tag: %i\n", node0->filetype.tag);
    if (memcmp(node0->name.name, orig, 8) != 0)
        log_and_die("Filename mismatch\n");
    node1 = haggis_create_node("output/Makefile", sha256, map, &mq);
    if (node1 == NULL)
        log_and_die("OOM creating node1\n");
    if (node1->filetype.tag != hardlink)
        log_and_die("Node 1 incorrect tag: %u\n", node1->filetype.tag);
    if (memcmp(node1->name.name, lnk, 15) != 0)
        log_and_die("Node 1 incorrect filename\n");
    ret = haggis_extract_node(node0, "output/extracted", &mq);
    if (ret)
        log_and_die("Error extracting node0: %i\n", ret);
    haggis_node_deinit(node0);
    ret = haggis_extract_node(node1, "output/extracted", &mq);
    if (ret)
        log_and_die("Error extracting node1: %i\n", ret);
    haggis_node_deinit(node1);
    msg = haggis_mq_pop(&mq);
    if (msg->tag != NodeCreated)
        log_and_die("Incorrect message, expected \"NodeCreated\", got %u\n", msg->tag);
    if (memcmp(msg->body.f_name, orig, 9) != 0)
        log_and_die("Filename mismatch: expected %s, got %s\n", orig, msg->body.f_name);
    haggis_msg_deinit(msg);
    msg = haggis_mq_pop(&mq);
    if (msg->tag != NodeCreated)
        log_and_die("Incorrect message, expected \"NodeCreated\", got %u\n", msg->tag);
    if (memcmp(msg->body.f_name, lnk, 15) != 0)
        log_and_die("Filename mismatch: expected %s, got %s\n", orig, msg->body.f_name);
    haggis_msg_deinit(msg);
    msg = haggis_mq_pop(&mq);
    if (msg->tag != NodeExtracted)
        log_and_die("Incorrect message, expected \"NodeExtracted\", got %u\n", msg->tag);
    if (memcmp(msg->body.f_name, orig, 9) != 0)
        log_and_die("Filename mismatch: expected %s, got %s\n", lnk, msg->body.f_name);
    haggis_msg_deinit(msg);
    msg = haggis_mq_pop(&mq);
    if (msg->tag != NodeExtracted)
        log_and_die("Incorrect message, expected \"NodeExtracted\", got %u\n", msg->tag);
    if (memcmp(msg->body.f_name, lnk, 15) != 0)
        log_and_die("Filename mismatch: expected %s, got %s\n", orig, msg->body.f_name);
    haggis_msg_deinit(msg);
    haggis_linkmap_deinit(map);
    return 0;
}
