#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mq.h"

int main() {
    haggis_mq mq;
    haggis_msg *m1, *m2, *m3, *m4;
    haggis_message_type mt1, mt2;
    haggis_message_body mb1, mb2;

    assert(haggis_mq_init(&mq) == 0);

    mt1 = NodeCreated;
    mt2 = EndOfArchive;
    mb1.f_name = "/bin/cat";
    mb2.f_name = NULL;

    m1 = haggis_msg_init(mt1, mb1);
    assert(m1 != NULL);
    assert(m1->next == NULL);
    assert(m1->prev == NULL);
    assert(m1->tag == NodeCreated);
    assert(memcmp(m1->body.f_name, "/bin/cat", 8) == 0);

    m2 = haggis_msg_init(mt2, mb2);
    assert(m2 != NULL);
    assert(m2->next == NULL);
    assert(m2->prev == NULL);
    assert(m2->tag == EndOfArchive);
    assert(m2->body.f_name == NULL);

    assert(haggis_mq_push(&mq, m1) == 0);
    assert(mq.head->tag == NodeCreated);
    assert(mq.tail->tag == NodeCreated);
    assert(haggis_mq_push(&mq, m2) == 0);
    assert(mq.head->tag == NodeCreated);
    assert(mq.tail->tag == EndOfArchive);

    m3 = haggis_mq_pop(&mq);
    assert(m3->tag == NodeCreated);
    assert(memcmp(m3->body.f_name, "/bin/cat", 8) == 0);
    free(m3);

    m4 = haggis_mq_pop(&mq);
    assert(m4->tag == EndOfArchive);
    assert(mq.head != NULL);
    free(m4);
    return 0;
}