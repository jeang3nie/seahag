# SeaHag
This is a C implementation of the [haggis](Format.md) archive format. It does not
follow any particular C standard (c99, c11 etc) but has been written with portability
in mind, being developed simultaneously on FreeBSD and Linux. It is implemented as
a library and by default both shared and static versions will be built.

## Building
Do a quick check of **config.mk** to be ensure the variables set therein work
for your system, and edit as needed.

SeaHag is built using a portable Makefile which works with both BSD and GNU Make.
Due to the limited subset of features used it *may* be portable to other versions
of Make, but that is untested as is the code on systems other than BSD or Linux.
The default target builds both shared and static libraries. If you prefer to build
one or the other, use the `shared` or `static` targets. The Makefile also follows
the `Destdir` pattern for ease of packaging.

## Tests
There is a simple test harness which can be invoked via `make test`. If you wish
to port this software to an untested platform the tests will greatly assist in
that process.
> Note: if your OS uses **dash** as `/bin/sh` then the builtin echo command will not
> be up to the task of printing the escape sequences used by the test runner. In
> this case, be sure to set the `ECHO` variable to `/bin/echo` in the *config.mk*
> file.

## Contributing
Contributions are always welcome and can be made via pull request or `git send-email`,
sent to jeang3nie at hitchhiker-linux dot org. Coding style is four space indent,
no tabs, with function signatures on one line unless that line would go past the
long line marker in which case each parameter goes on a new line. The intention
is that the software should remain portable with no further dependencies beyond
libmd and libpthread, so contributions which introduce further dependencies will
be rejected. Of particular interest are contributions in the form of documentation
or porting to untested platforms.
